#!/bin/bash

PROJECT=$1

mkdir -p $PROJECT

git init $PROJECT

mkdir -p $PROJECT/src
mkdir -p $PROJECT/build
mkdir -p $PROJECT/tests

touch $PROJECT/README.md
touch $PROJECT/CHANGES.md

cp .gitignore $PROJECT/.gitignore

git --git-dir ./$PROJECT/.git --work-tree=./$PROJECT/ add --all
git --git-dir ./$PROJECT/.git/ --work-tree=./$PROJECT/ commit -m 'Initial commit. Prepare directory structure for $PROJECT.'

cd $PROJECT


