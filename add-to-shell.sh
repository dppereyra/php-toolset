#!/bin/bash

curl -sS https://getcomposer.org/installer | php
wget http://codeception.com/codecept.phar
wget http://www.phing.info/get/phing-latest.phar
wget https://phar.phpunit.de/phpunit.phar
wget https://github.com/sebastianbergmann/phpcpd/releases/download/2.0.0/phpcpd.phar
wget -c http://static.phpmd.org/php/latest/phpmd.phar

chmod +x *.phar

sudo cp composer.phar /usr/local/bin/composer
sudo cp codecept.phar /usr/local/bin/codecept
sudo cp phpunit.phar /usr/local/bin/phpunit
sudo cp phing-latest.phar /usr/local/bin/phing
sudo cp phpcpd.phar /usr/local/bin/phpcpd
sudo cp phpmd.phar /usr/local/bin/phpmd

rm *.phar
